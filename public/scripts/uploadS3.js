function ajaxCall(url,method,expectedStatus,successCb,errCb,formElement,params) {
  let request = new XMLHttpRequest();
  request.open(method,url, true);
  request.onload = function() {
    if(request.status !== expectedStatus) {
      errCb(request)
    } else {
      successCb(request)
    }
  };
  request.onerror=function(e) {
    errCb(request)
  }
  if(method=='GET' || method=='DELETE') {
    request.send()
  } else {
    let formData=new FormData(formElement)
    for(param in params) {
      formData.set(param,params[param])
    }
    let fileData=formData.get('file')
    formData.delete('file')
    formData.append('file',fileData)
    var object = {};
    formData.forEach(function(value, key){
      object[key] = value;
    });
    var json = JSON.stringify(object,null,2)
    console.log(json)

    request.send(formData)
  }
}

function getSignature(successCb,errCb,filename) {
  ajaxCall('/s3_credentials?filename='+filename,'GET',200,successCb,errCb)
}

function uploadS3(formElement,filename,successCb,errorCb) {
  function handleSignatureSuccess(sigRequest) {
    const response = JSON.parse(sigRequest.response)
    ajaxCall(
      response.endpoint_url,
      'POST',
      parseInt(response.params.success_action_status),
      successCb,errorCb,formElement,response.params)
  }
  function handleSignatureError(sigRequest) {
    console.log('Error getting signature:',JSON.stringify(sigRequest,null,2))
  }
  getSignature(handleSignatureSuccess,handleSignatureError,filename)
}

