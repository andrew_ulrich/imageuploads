function getSignatureGCS(successCb,errCb,filename) {
  ajaxCall('/gcs_signed_url?filename='+filename,'GET',200,successCb,errCb)
}

function uploadGCS(formElement,filename,successCb,errorCb) {
  function handleSignatureSuccess(sigRequest) {
    const response = JSON.parse(sigRequest.response)
    ajaxCall(
      response.endpoint_url,
      'PUT',
      response.params.success_action_status,
      successCb,errorCb,formElement,{})
  }
  function handleSignatureError(sigRequest) {
    console.log('Error getting signature:',JSON.stringify(sigRequest,null,2))
  }
  getSignatureGCS(handleSignatureSuccess,handleSignatureError,filename)
}

