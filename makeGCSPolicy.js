/**
 * upload to GCS using a signed policy document
 * see https://cloud.google.com/storage/docs/access-control/ for why you would want to use a signed policy document for your uploads
 * @type {number}
 */

const MAX_FILE_SIZE=400000 //in bytes
const crypto = require('crypto')

/**
 * make the base64 encoded policy defining what a user can upload
 * see https://cloud.google.com/storage/docs/xml-api/post-object#policydocument
 * @param bucket
 * @param filename
 * @param contentType
 * @returns {string}
 */
function makePolicy(bucket,filename,contentType) {
  return new Buffer(JSON.stringify({
    // 5 minutes into the future
    expiration: new Date((new Date).getTime() + (5 * 60 * 1000)).toISOString(),
    conditions: [
      // {"acl": "bucket-owner-read" }, //not sure if I need this
      {bucket:bucket},
      {key: filename},
      {success_action_status: 201 },
      ["eq", "$Content-Type", contentType ],
      ["content-length-range", 0, MAX_FILE_SIZE]
    ]
  })).toString('base64')
}

/**
 * make the signature using sha256 and output in base64 format
 * signs with the pem created from the pkcs12 service account key using https://cloud.google.com/storage/docs/authentication#converting-the-private-key
 * see https://nodejs.org/api/crypto.html#crypto_class_sign
 * @param encodedPolicy
 * @param pemKey
 * @returns {number}
 */
function makeSignature(encodedPolicy,pemKey) {
  // const messageDigest=crypto.createHmac('sha256',secretKey).update(encodedPolicy).digest('base64')
  const sign = crypto.createSign('RSA-SHA256');
  sign.update(encodedPolicy)
  const messageDigest = sign.sign(pemKey,'base64')
  return messageDigest
}

/**
 * makes a json object containing all the form data needed (except the file itself) to upload to s3 using a signed policy
 * see https://cloud.google.com/storage/docs/xml-api/post-object
 * @param bucket
 * @param filename
 * @param contentType
 * @param accessId
 * @param pemKey
 * @returns {{key: *, bucket: *, Content-Type: *, GoogleAccessId: *, success_action_status: number, policy, signature}}
 */
function getFormDataAsObject(bucket,filename,contentType,accessId,pemKey) {
  let encodedPolicy=makePolicy(bucket,filename,contentType)
  return {
    key:filename,
    bucket,
    "Content-Type":contentType,
    GoogleAccessId:accessId,
    success_action_status:201,
    policy:encodedPolicy,
    signature:makeSignature(encodedPolicy,pemKey)
  }
}

/**
 * gets the url to upload to
 * see https://cloud.google.com/storage/docs/request-endpoints
 * @param bucket
 * @returns {string}
 */
function getUrl(bucket) {
  return `https://www.googleapis.com/upload/storage/v1/b/${bucket}/o`
}
module.exports={
  getFormDataAsObject,
  getUrl
}