const express = require('express')
const crypto = require('crypto')
const path = require('path')

const s3 = require('./makeS3Policy')
const gcs=require('./makeGCSPolicy')
const signedUrlsGcs=require('./signedUrlsGcs')

const s3Config = {
  accessKey: process.env.S3_ACCESS_KEY,
  secretKey: process.env.S3_SECRET_KEY,
  bucket: process.env.S3_BUCKET,
  region: process.env.S3_REGION
};

function getPemFromLocal() {
  const fs=require('fs')
  return fs.readFileSync('misc/privateKey.pem','utf8')
}
const gcsConfig = {
  bucket: process.env.GOOGLE_CS_BUCKET,
  projectId: process.env.GOOGLE_CS_PROJECT,
  accessId: process.env.GOOGLE_CS_ACCESS_ID,
  pemKey: process.env.NODE_ENV=='ngrok' ? getPemFromLocal() : process.env.GCS_PEM_KEY
}
console.log(gcsConfig)
const app = express();

app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

app.get('/s3_credentials', (req, res) => {
  if (req.query.filename) {
    const filename =
      (new Date()).getTime()+'-' + //collisions are now even more impossible (paranoia)
      crypto.randomBytes(16).toString('hex') +
      path.extname(req.query.filename);
    res.json(s3.s3Credentials(s3Config, {filename: filename, contentType: 'image/jpeg'}));
  } else {
    res.status(400).send('filename is required');
  }
});

app.get('/gcs_credentials', (req, res) => {
  if (req.query.filename) {
    const filename =
      (new Date()).getTime()+'-' + //collisions are now even more impossible (paranoia)
      crypto.randomBytes(16).toString('hex') +
      path.extname(req.query.filename);
    res.json({
      params: gcs.getFormDataAsObject(
        gcsConfig.bucket,
        filename,
        'image/jpeg',
        gcsConfig.accessId, //https://cloud.google.com/compute/docs/access/create-enable-service-accounts-for-instances
        gcsConfig.pemKey),
      endpoint_url: gcs.getUrl(gcsConfig.bucket)
    })
  } else {
    res.status(400).send('filename is required');
  }
});

app.get('/gcs_signed_url', (req, res) => {
  if (req.query.filename) {
    const filename =
      (new Date()).getTime()+'-' + //collisions are now even more impossible (paranoia)
      crypto.randomBytes(16).toString('hex') +
      path.extname(req.query.filename);
    res.json({
      params: {
        success_action_status:200
      },
      endpoint_url: signedUrlsGcs(gcsConfig.bucket,filename,gcsConfig.accessId,gcsConfig.pemKey),
      filename:filename
    })
  } else {
    res.status(400).send('filename is required');
  }
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});