/**
 * make signed urls for google cloud storage
 *
 *
 */
const MAX_FILE_SIZE=1000000 //in bytes
const crypto = require('crypto')



function getSignatureString(bucket, filename,expiration) {
  const method='PUT'
  const contentType='image/jpeg'
  //for the canonical headers, see https://cloud.google.com/storage/docs/xml-api/reference-headers
  //todo add x-goog-meta-userid to canonicalHeaders
  const canonicalHeaders=[`x-goog-content-length-range:0,${MAX_FILE_SIZE}`].join('\n')

  return `${method}\n\n${contentType}\n${expiration}\n${canonicalHeaders}\n/${bucket}/${filename}`
}

function makeSignedUrl(bucket,filename,accessId,privateKey) {
  // 5 minutes into the future
  const expiration=(new Date()).getTime() + (5 * 60 * 1000)
  const rawString=getSignatureString(bucket,filename,expiration)
  return `https://storage.googleapis.com/${bucket}/${filename}?`
  +`GoogleAccessId=${accessId}&Expires=${expiration}&Signature=${makeSignatureForUrl(rawString,privateKey)}`
}


/**
 * make the signature using sha256 and output in base64 format
 * signs with the pem created from the pkcs12 service account key using https://cloud.google.com/storage/docs/authentication#converting-the-private-key
 * see https://nodejs.org/api/crypto.html#crypto_class_sign
 * @param contents
 * @param pemKey
 * @returns {number}
 */
function makeSignatureForUrl(contents,pemKey) {
  const sign = crypto.createSign('RSA-SHA256');
  sign.update(contents)
  const messageDigest = sign.sign(pemKey,'base64')
  return encodeURIComponent(messageDigest)
}
module.exports=makeSignedUrl